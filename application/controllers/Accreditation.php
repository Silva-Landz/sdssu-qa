<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Accreditation extends CI_Controller   {
    
    public function __construct()   {

        parent::__construct();

        $this->load->model('program_model');
        $this->load->model('others_model');

    }

    public function templates($path, $data = NULL) {

        if($this->session->userdata('isLoggedIn') == 1 || $this->uri->segment(2) == 'login' || $this->uri->segment(2) == 'register')    {

            $this->load->view('includes/header', $data);
            $this->load->view($path, $data);
            $this->load->view('includes/footer');

        } else {

            show_404();

        }

    }

    public function programs()  {

        $data['title'] = "Programs";
        $data['programs'] = $this->program_model->get_all();

        $this->templates('main/programs', $data);

    }

    public function others()   {

        $data['title'] = "PPP, Self survey, Narrative Report and Results of Accreditation";
        $data['others'] = $this->others_model->get_all();

        $this->templates('main/others', $data);

    }

    public function phases()    {

        $data['title'] = "Phase";
        $data['programs'] = $this->program_model->get_all();

        $this->templates('main/phases', $data);

    }

}
