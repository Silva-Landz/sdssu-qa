<div class="container-fluid background"></div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12"></div>
        <div class="col-md-4 col-sm-4 col-xs-12 p-5">
            <?= $this->session->userdata('success'); ?>
            <form action="<?= base_url('admin/register'); ?>" method="POST">
                <div class="card">
                    <div class="card-body border-dark pl-4 pr-4">
                        <div class="form-group">
                            <label style="font-size:1.5rem;"><b>Create a New Account</b></label><br>
                            <small class="text-muted"><b>NOTE:</b> All fields are required to fill up.</small>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="firstname" value="<?= set_value('firstname'); ?>" placeholder="Firstname">
                            <small class="text-danger"><?= form_error('firstname'); ?></small>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="lastname" value="<?= set_value('lastname'); ?>" placeholder="Lastname">
                            <small class="text-danger"><?= form_error('lastname'); ?></small>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="username" value="<?= set_value('username'); ?>" placeholder="Username">
                            <small class="text-danger"><?= form_error('username'); ?></small>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="Password">
                            <small class="text-danger"><?= form_error('password'); ?></small>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="confpass" placeholder="Confirm password">
                            <small class="text-danger"><?= form_error('confpass'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for=""><b>Usertype</b></label>
                            <select name="usertype" class="form-control">
                                <option value="">Choose . . .</option>
                                <option value="administrator">Administrator</option>
                                <option value="staff">Staff</option>
                            </select>
                            <small class="text-danger"><?= form_error('usertype'); ?></small>
                        </div>
                        <button class="btn btn-success form-control mt-2">Sign up</button>
                        <p class="mt-3 text-center">Already registered? <a href="<?= base_url('admin/login'); ?>">Login</a></p>
                    </div>
                </div>  
            </form>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12"></div>
    </div>
</div>