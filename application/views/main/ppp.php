<?php $users = $this->session->userdata('users'); ?>

<?php foreach($users as $user): ?>

<div class="jumbotron jumbotron-fluid bg-primary text-white">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <h1>PPP</h1>
            </div>
        </div>
    </div>
</div>

<!-- adding a programs modal -->
<div class="modal fade create-ppp" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h5 class="modal-title text-white">Create PPP</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">PPP title</label>
                        <input type="text" class="form-control" placeholder="Enter ppp's title">
                    </div>
                    <div class="form-group">
                        <label for="">Upload file</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Upload</span>
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="inputGroupFile01">
                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>  
            </form>  
        </div>
    </div>
</div>
<!-- End modal -->

<div class="container mt-5">
    <div class="row">
        <div class="col-md-12">
            <a href="#" class="btn btn-success" data-toggle="modal" data-target=".create-ppp"><span class="fa fa-plus-circle"></span> Add PPP</a>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md-12">
            <table id="programs" class="table table-striped table-hovers" style="width:100%;">
                <thead class="bg-secondary text-white">
                    <tr class="text-center">
                        <th>ID</th>
                        <th>PPP Description</th>
                        <th>File thumbnail</th>
                        <th>View</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="text-center">
                        <td>1</td>
                        <td>Bachelor's of Science in Computer Science</td>
                        <td>College of Engineering, Computer Studies and Technology</td>
                        <td><a href="#" class="btn btn-info btn-sm" title="View"><i class="fa fa-info-circle"></a></td>
                        <td><a href="#" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></a></td>
                        <td><a href="#" class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-trash"></a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php endforeach; ?>
