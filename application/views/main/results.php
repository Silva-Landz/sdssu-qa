<?php $users = $this->session->userdata('users'); ?>

<?php foreach($users as $user): ?>

<div class="jumbotron jumbotron-fluid bg-primary text-white">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <h1>Results of Accreditation</h1>
            </div>
        </div>
    </div>
</div>

<!-- adding a programs modal -->
<div class="modal fade create-results" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create Accreditation Results</h5>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer"></div>    
        </div>
    </div>
</div>
<!-- End modal -->

<div class="container mt-5">
    <div class="row">
        <div class="col-md-12">
            <a href="#" class="btn btn-success" data-toggle="modal" data-target=".create-results"><span class="fa fa-plus-circle"></span> Add Results</a>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md-12">
            <table id="programs" class="table table-striped table-hovers" style="width:100%;">
                <thead class="bg-secondary text-white">
                    <tr class="text-center">
                        <th>ID</th>
                        <th>Description</th>
                        <th>File</th>
                        <th>Download</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="text-center">
                        <td>1</td>
                        <td>This the sample result</td>
                        <td>File thumbnail</td>
                        <td><a href="#" class="btn btn-info btn-sm" title="View"><i class="fa fa-download"></i></a></td>
                        <td><a href="#" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a></td>
                        <td><a href="#" class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-trash"></i></a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php endforeach; ?>
