<?php $users = $this->session->userdata('users'); ?>

<?php foreach($users as $user): ?>

<div class="jumbotron jumbotron-fluid bg-primary text-white">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <?php if($user->usertype == 'administrator'): ?>
                    <h1>Welcome Administrator</h1>
                <?php else: ?>   
                    <h1>Welcome Staff</h1> 
                <?php endif;?>
            </div>
        </div>
    </div>
</div>

<div class="container mt-5">
    <div class="row">
        <div class="col-md-3 pb-2 mb-3" style="border-bottom: solid red;">
            <h4><i class="fa fa-graduation-cap"></i> Degree Programs</h4>
        </div>
        <div class="col-md-12">
            <?php foreach($courses as $course): ?>
                <ul>
                    <li><a href="<?= base_url('areas/view/'). $course->course_id; ?>" class="text-secondary"><?= $course->course_name." (". $course->course_code .")"; ?></a></li>
                </ul>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<?php endforeach; ?>
