-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 03, 2018 at 02:18 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qadb`
--

-- --------------------------------------------------------

--
-- Table structure for table `sqa_areas`
--

CREATE TABLE `sqa_areas` (
  `area_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `vission` text,
  `mission` text,
  `goals` text,
  `objectives` text,
  `faculties` text,
  `curriculum_instructions` text,
  `student_services` text,
  `research_development` text,
  `extension` text,
  `library` text,
  `physical_facilities` text,
  `laboratory` text,
  `administration` text,
  `ppp_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sqa_college`
--

CREATE TABLE `sqa_college` (
  `college_id` int(11) NOT NULL,
  `college_code` varchar(50) NOT NULL,
  `college_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sqa_college`
--

INSERT INTO `sqa_college` (`college_id`, `college_code`, `college_name`) VALUES
(1, 'CEBM', 'College of Education and Business Management'),
(2, 'CECST', 'College of Engineering and Computer Science Technology');

-- --------------------------------------------------------

--
-- Table structure for table `sqa_course`
--

CREATE TABLE `sqa_course` (
  `course_id` int(11) NOT NULL,
  `course_code` varchar(50) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `college_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sqa_course`
--

INSERT INTO `sqa_course` (`course_id`, `course_code`, `course_name`, `college_id`) VALUES
(1, 'BSED - Sciences', 'Bachelor of Secondary Education major in Sciences', 1),
(2, 'BSED - TLE', 'Bachelor of Secondary Education major in Technology & Livelihood Education', 1),
(3, 'BSED - Mathematics', 'Bachelor of Secondary Education major in Mathematics', 1),
(4, 'BTTE - Architectural Drafting', 'Bachelor of Technical Teacher Education major in Architectural Drafting', 1),
(5, 'BTTE - Civil', 'Bachelor of Technical Teacher Education major in Civil', 1),
(6, 'BTTE - Foods', 'Bachelor of Technical Teacher Education major in Foods', 1),
(7, 'BTTE - Electricity', 'Bachelor of Technical Teacher Education major in Electricity', 1),
(8, 'BTTE - Automotive', 'Bachelor of Technical Teacher Education major in Automotive', 1),
(9, 'BTTE - Electronics ', 'Bachelor of Technical Teacher Education major in Electronics', 1),
(10, 'BTTE - Garments', 'Bachelor of Technical Teacher Education major in Garments', 1),
(11, 'BTTE - Welding Fabrication', 'Bachelor of Technical Teacher Education major in Welding and Fabrication', 1),
(12, 'BSIT - Architectural Drafting', 'Bachelor of Science in Industrial Technology major in Architectural Drafting', 2),
(13, 'BSIT - Civil Technology', 'Bachelor of Science in Industrial Technology major in Civil Technology', 2),
(14, 'BSIT - Electricity Technology', 'Bachelor of Science in Industrial Technology major in Electricity Technology', 2),
(15, 'BSIT - Electronics Technology', 'Bachelor of Science in Industrial Technology major in Electronics Technology', 2),
(16, 'BSIT - Food Technology', 'Bachelor of Science in Industrial Technology major in Foods Technology', 2),
(17, 'BSIT - Garments Technology', 'Bachelor of Science in Industrial Technology major in Garments Technology', 2),
(18, 'BSIT - Hotel and Restaurant Servicing', 'Bachelor of Science in Industrial Technology major in Hotel & Restaurant Servicing', 2),
(19, 'BSIT - Mechanical Technology', 'Bachelor of Science in Industrial Technology major in Mechanical Technology', 2),
(20, 'BSIT - Welding and Fabrication', 'Bachelor of Science in Industrial Technology major in Welding and Fabrication', 2),
(21, 'BSCS', 'Bachelor of Science in Computer Science', 2),
(22, 'BSHRM', 'Bachelor of Science in Hotel and Restaurant Management', 1),
(23, 'BS Info. Tech.', 'Bachelor of Science in Information Technology', 2),
(24, 'BSBA - Financial Management', 'Bachelor of Science in Business Administration major in Financial Management', 1),
(25, 'BSCE', 'Bachelor of Science in Computer Engineering', 2),
(26, 'BSBA - Human Resource Development Management', 'Bachelor of Science in Business Administration major in Human Resource Development Management', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sqa_narrative`
--

CREATE TABLE `sqa_narrative` (
  `report_id` int(11) NOT NULL,
  `report_title` varchar(255) NOT NULL,
  `report_upload` varchar(255) NOT NULL,
  `registered_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sqa_ppp`
--

CREATE TABLE `sqa_ppp` (
  `ppp_id` int(11) NOT NULL,
  `ppp_title` varchar(255) NOT NULL,
  `ppp_upload` varchar(255) NOT NULL,
  `registered_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sqa_programs`
--

CREATE TABLE `sqa_programs` (
  `prog_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sqa_survey`
--

CREATE TABLE `sqa_survey` (
  `survey_id` int(11) NOT NULL,
  `survey_title` varchar(255) NOT NULL,
  `survey_upload` varchar(255) NOT NULL,
  `registered_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sqa_users`
--

CREATE TABLE `sqa_users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `usertype` enum('administrator','staff') NOT NULL,
  `registered_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sqa_users`
--

INSERT INTO `sqa_users` (`id`, `firstname`, `lastname`, `username`, `password`, `usertype`, `registered_on`) VALUES
(1, 'Justin', 'Apatan', 'apatanjustin', '2f4aa9c4960b53c6e3a113385f3457fa87021f88c9e1c20b5cb4a6c31167940b21cffc0382d90a334723ef9ff85f9ac1f24712067497abc6162084f2d770f507', 'administrator', '2018-08-02 18:12:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sqa_areas`
--
ALTER TABLE `sqa_areas`
  ADD PRIMARY KEY (`area_id`);

--
-- Indexes for table `sqa_college`
--
ALTER TABLE `sqa_college`
  ADD PRIMARY KEY (`college_id`);

--
-- Indexes for table `sqa_course`
--
ALTER TABLE `sqa_course`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `sqa_narrative`
--
ALTER TABLE `sqa_narrative`
  ADD PRIMARY KEY (`report_id`);

--
-- Indexes for table `sqa_ppp`
--
ALTER TABLE `sqa_ppp`
  ADD PRIMARY KEY (`ppp_id`);

--
-- Indexes for table `sqa_programs`
--
ALTER TABLE `sqa_programs`
  ADD PRIMARY KEY (`prog_id`);

--
-- Indexes for table `sqa_survey`
--
ALTER TABLE `sqa_survey`
  ADD PRIMARY KEY (`survey_id`);

--
-- Indexes for table `sqa_users`
--
ALTER TABLE `sqa_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sqa_areas`
--
ALTER TABLE `sqa_areas`
  MODIFY `area_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sqa_college`
--
ALTER TABLE `sqa_college`
  MODIFY `college_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sqa_course`
--
ALTER TABLE `sqa_course`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `sqa_narrative`
--
ALTER TABLE `sqa_narrative`
  MODIFY `report_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sqa_ppp`
--
ALTER TABLE `sqa_ppp`
  MODIFY `ppp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sqa_programs`
--
ALTER TABLE `sqa_programs`
  MODIFY `prog_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sqa_survey`
--
ALTER TABLE `sqa_survey`
  MODIFY `survey_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sqa_users`
--
ALTER TABLE `sqa_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
