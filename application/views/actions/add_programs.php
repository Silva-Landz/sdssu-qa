<form action="<?= base_url('programs/create'); ?>" method="POST" enctype="multipart/form-data">    
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="card border-success">
                    <div class="card-header bg-success text-white" style="border-radius:0px">
                        <b>Programs</b>
                    </div>
                    <div class="card-body">
                        <label for=""><b>Degree Program</b></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">Options</label>
                            </div>
                            <select class="custom-select" name="courses">
                                <option value="">Choose...</option>
                                <?php if(is_object($courses) || is_array($courses)):?>
                                    <?php foreach($courses as $course): ?>
                                        <option value="<?= $course->course_id; ?>"><?= $course->course_name; ?></option>
                                    <?php endforeach; ?>
                                <?php endif;?>
                            </select>
                        </div>
                        <small class="form-text text-danger"><b><?= form_error('courses'); ?></b></small>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for=""><b>AREAS</b></label>
                                <select name="areas" id="" class="form-control">
                                    <option value="">Choose...</option>
                                    <?php if(is_object($areas) || is_array($areas)):?>
                                        <?php foreach($areas as $area): ?>
                                            <option value="<?= $area->area_id; ?>"><?= $area->area_name; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif;?>
                                </select>
                                <small class="form-text text-danger"><b><?= form_error('areas'); ?></b></small>
                            </div>

                            <div class="col-md-3">
                                <label for=""><b>Level</b></label>
                                <select name="levels" class="form-control">
                                    <option value="">Choose</option>
                                    <option value="1">Level 1</option>
                                    <option value="2">Level 2</option>
                                    <option value="3">Level 3</option>
                                    <option value="4">Level 4</option>
                                </select>
                                <small class="form-text text-danger"><b><?= form_error('levels'); ?></b></small>
                            </div>
                            <div class="col-md-3">
                                <label for=""><b>Phase</b></label>
                                <select name="phases" class="form-control">
                                    <option value="">Choose</option>
                                    <option value="0">Preliminary</option>
                                    <option value="1">Phase 1</option>
                                    <option value="2">Phase 2</option>
                                </select>
                                <small class="form-text text-danger"><b><?= form_error('phases'); ?></b></small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for=""><b>PPP File</b></label>
                                <select name="ppp" class="form-control">
                                    <option value="">Choose</option>
                                    <?php if(is_object($ppp) || is_array($ppp)):?>
                                        <?php foreach($ppp as $p): ?>
                                            <?php if($p->cat_id == 1):?>
                                                <option value="<?= $p->other_id; ?>"><?= $p->other_name; ?></option>
                                            <?php endif;?>
                                        <?php endforeach; ?>
                                    <?php endif;?>
                                </select>
                                <small class="form-text text-danger"><b><?= form_error('ppp'); ?></b></small>
                            </div>
                        </div>

                        <div class="form-group mt-5">
                            <label for=""><i class="fa fa-upload"></i> <b>Upload file</b></label>
                            <input type="file" class="form-control" name="fileupload">
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary pull-right">Submit</button>
                        <a href="<?= base_url('accreditation/programs'); ?>" class="btn btn-light mr-2 pull-right">Cancel</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</form>