<?php $users = $this->session->userdata('users'); ?>

<?php foreach($users as $user): ?>

<div class="jumbotron jumbotron-fluid bg-primary text-white">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <h1>PPP, Self Survey, Narrative Report and Results of Accreditation</h1>
            </div>
        </div>
    </div>
</div>


<div class="container mt-5">
    <div class="row">
        <div class="col-md-12">
            <?= $this->session->flashdata('message'); ?>
            <a href="<?= base_url('others/create'); ?>" class="btn btn-success" ><span class="fa fa-plus-circle"></span> Add Entry</a>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md-12">
            <table id="programs" class="table table-striped table-hovers" style="width:100%;">
                <thead class="bg-secondary text-white">
                    <tr class="text-center">
                        <th>ID</th>
                        <th>File Description</th>
                        <th>File Category</th>
                        <th>Level and Phase</th>
                        <th>View</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(is_object($others) || is_array($others)):?>
                        <?php foreach($others as $other): ?>
                        <tr>
                            <td><?= $other->other_id; ?></td>
                            <td><?= $other->other_name; ?></td>
                            <td><?= $other->cat_name; ?></td>
                            <td>
                                <?php
                                    if($other->other_phase == 0)    {
                                        echo "Preliminary";
                                    } else  {
                                        echo "Level " .$other->other_level. " - Phase " .$other->other_phase;
                                    }
                                ?>  
                            </td>
                            <td><a href="<?= base_url('others/view/'). $other->other_id; ?>" class="btn btn-info btn-sm" title="View"><i class="fa fa-info-circle"></a></td>
                            <td><a href="<?= base_url('others/edit/'). $other->other_id; ?>" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></a></td>
                            <td>
                                <a href="javascript:void(0)" class="btn btn-danger btn-sm" id="delete_file" data-id="<?= $other->other_id; ?>"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    <?php endif;?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php endforeach; ?>
