<div class="container-fluid background">
    <div class="row login-form">
        <div class="col-lg-4 col-md-6 col-sm-10 col-xs-12 mx-auto">
            <form action="<?= base_url('admin/login'); ?>" method="POST">
                <div class="form-group">
                    <?= validation_errors('<p class="alert alert-danger"><span class="fa fa-exclamation-circle"></span> ', '</p>'); ?>
                    <h1 class="text-white"><i class="fa fa-file-archive-o"></i> <b>Log in</b></h1>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control form-control-lg" name="username" placeholder="Username">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control form-control-lg" name="password" placeholder="Password">
                </div>
                <div class="form-group">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="inputGroupSelect01">Options</label>
                        </div>
                        <select class="custom-select form-control form-control-lg" name="usertype">
                            <option value="administrator">Administrator</option>
                            <option value="staff">Staff</option>
                        </select>
                    </div>
                </div>
                <button class="btn btn-primary btn-lg form-control">Login</button>
            </form>
        </div>
    </div>
</div>