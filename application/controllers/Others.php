<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Others extends CI_Controller    {
    
    public function __construct()   {

        parent::__construct();

        $this->load->model('others_model');

    }

    public function templates($path, $data = NULL) {

        if($this->session->userdata('isLoggedIn') == 1 || $this->uri->segment(2) == 'login' || $this->uri->segment(2) == 'register')    {

            $this->load->view('includes/header', $data);
            $this->load->view($path, $data);
            $this->load->view('includes/footer');

        } else {

            show_404();

        }

    }

    public function create()    {

        $data['title'] = "Create new entry";
        $data['categories'] = $this->db->get('sqa_categories')->result();

        $this->form_validation->set_rules('description', 'description', 'required');
        $this->form_validation->set_rules('categories', 'category', 'required');

        if($this->input->post('categories') == 1)    {
            $this->form_validation->set_rules('levels', 'level', 'required');
            $this->form_validation->set_rules('phases', 'phase', 'required');
        }

        // upload config
        $file_path = realpath(APPPATH. '../uploads/files/others');

        $config['upload_path'] = $file_path;
        $config['allowed_types'] = 'pdf';
        $config['remove_spaces'] = true;

        $this->upload->initialize($config);
        // end upload

        if($this->form_validation->run() == FALSE)    {

            $this->templates('actions/add_others', $data);

        } else  {
            $uploaded = $this->upload->do_upload('others_upload');

            if(!$uploaded)    {

                var_dump($uploaded); die();
                $message = $this->upload->display_errors('<p class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ', '</p>');

            } else  {

                if($this->others_model->create($this->upload->data()))    {

                    $message = "<p class='alert alert-success'><i class='fa fa-check-circle'></i> Successfully added a new program.</p>";
                    
                } else{

                    $message = "<p class='alert alert-danger'>Failed to add a new program</p>";

                }

            }

            $this->session->set_flashdata('message', $message);
            redirect('accreditation/others', 'refresh');
        }

    }

    public function view($id = NULL)  {

        if(is_numeric($id) && !is_null($id))    {
        
            $data['title'] = "View Details";
            $data['others'] = $this->others_model->get_specific($id);

            $this->templates('actions/view_others', $data);

        } else  {

            show_404();

        }

    }

    public function edit($id = NULL)  {

        $this->form_validation->set_rules('description', 'description', 'required');
        $this->form_validation->set_rules('categories', 'category', 'required');

        if($this->input->post('categories') == 1)    {
            $this->form_validation->set_rules('levels', 'level', 'required');
            $this->form_validation->set_rules('phases', 'phase', 'required');
        }

        // upload config
        $file_path = realpath(APPPATH. '../uploads/files/others');

        $config['upload_path'] = $file_path;
        $config['allowed_types'] = 'pdf';
        $config['remove_spaces'] = true;

        $this->upload->initialize($config);
        // end upload

        if(is_numeric($id) && !is_null($id))    {
        
            $data['title'] = "View Details";
            $data['others'] = $this->others_model->get_specific($id);
            $data['categories'] = $this->db->get('sqa_categories')->result();

            if($this->form_validation->run() == FALSE)    {

                $this->templates('actions/edit_others', $data);

            } else  {
                if(!$this->upload->do_upload('others_upload'))    {

                    if($this->others_model->update_failed($id))    {
    
                        $message = "<p class='alert alert-success'>Successfully updated the program.</p>";
                        
                    } else{
    
                        $message = "<p class='alert alert-danger'>Failed to update the program</p>";
    
                    }
    
                } else  {
    
                    if($this->others_model->update($id))    {
    
                        $message = "<p class='alert alert-success'>Successfully updated the program.</p>";
                        
                    } else{
    
                        $message = "<p class='alert alert-danger'>Failed to update the program</p>";
    
                    }
    
                }
    
                $this->session->set_flashdata('message', $message);
                redirect('accreditation/others', 'refresh');
            }

        } else  {

            show_404();

        }

    }

    public function delete()    {

        $id = intval($this->input->post('fid', TRUE));
        $file_path = realpath(APPPATH. '../uploads/files/others');

        if(is_numeric($id) && !is_null($id))    {

            $data['others'] = $this->others_model->get_specific($id);

            if($this->others_model->delete_file($id))    {

                foreach($data['others'] as $other)   {

                    $fileToDelete = $file_path. "\\" .$other->other_fileupload;
                    unlink($fileToDelete);

                }

                $response['status'] = 'success';
                $response['message'] = 'Dispense deleted successfully';

            } else  {

                $response['status'] = 'error';
                $response['message'] = 'Unable to delete dispense';

            }


        } else  {

            show_404();

        }

        echo json_encode($response);

    }

}