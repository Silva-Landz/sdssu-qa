<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model  {

    public function __construct()   {

        parent::__construct();

    }

    public function login_account() {

        $username = $this->input->post('username');
        $password = $this->hashed_password();
        $usertype = $this->input->post('usertype');

        $query = $this->db->where('username', $username)
                 ->where('password', $password)
                 ->where('usertype', $usertype)
                 ->get('sqa_users');
        
        if($query->num_rows() == 1)    {

            return $query->result();

        } else  {

            return FALSE;

        }
    }

    public function register_account()  {

        $data = array(
            'firstname' => $this->input->post('firstname', TRUE),
            'lastname'  => $this->input->post('lastname', TRUE),
            'username'  => $this->input->post('username', TRUE),
            'password'  => $this->hashed_password(),
            'usertype'  => $this->input->post('usertype', TRUE)
        );

        $this->db->insert('sqa_users', $data);

        return TRUE;

    }

    public function hashed_password()   {

        // hash password using sha512 with salt
        $password = $this->input->post('password'. TRUE);
        $key = $this->config->item('encryption_key');
        $salt1 = hash('sha512', $key . $password);
        $salt2 = hash('sha512', $password . $key);
        $hashed_password = hash('sha512', $salt1 . $password . $salt2);

        return $hashed_password;
    }

}
