<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Programs extends CI_Controller    {
    
    public function __construct()   {

        parent::__construct();

        $this->load->model('program_model');

    }

    public function templates($path, $data = NULL) {

        if($this->session->userdata('isLoggedIn') == 1 || $this->uri->segment(2) == 'login' || $this->uri->segment(2) == 'register')    {

            $this->load->view('includes/header', $data);
            $this->load->view($path, $data);
            $this->load->view('includes/footer');

        } else {

            show_404();

        }

    }

    public function create()    {

        $data['title'] = "Add Program";
        $data['courses'] = $this->program_model->get_course();
        $data['areas'] = $this->db->get('sqa_areas')->result();
        $data['ppp'] = $this->db->get('sqa_others')->result();

        $this->form_validation->set_rules('courses', 'degree program', 'required');
        $this->form_validation->set_rules('areas', 'areas', 'required');

        if($this->input->post('phases') != 0)    {
            $this->form_validation->set_rules('levels', 'level', 'required');
        }   
        
        // upload config
        $file_path = realpath(APPPATH. '../uploads/files');

        $config['upload_path'] = $file_path;
        $config['allowed_types'] = 'pdf';
        $config['remove_spaces'] = true;

        $this->upload->initialize($config);
        // end upload

        if($this->form_validation->run() == FALSE)    {

            $this->templates('actions/add_programs', $data);

        } else  {

            if(!$this->upload->do_upload('fileupload'))    {

                $message = $this->upload->display_errors('<p class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ', '</p>');

            } else  {

                if($this->program_model->create($this->upload->data()))    {

                    $message = "<p class='alert alert-success'><i class='fa fa-check-circle'></i> Successfully added a new program.</p>";
                    
                } else{

                    $message = "<p class='alert alert-danger'>Failed to add a new program</p>";

                }

            }

            $this->session->set_flashdata('message', $message);
            redirect('accreditation/programs', 'refresh');
        }

    }

    public function view($id = NULL)  {

        if(is_numeric($id) && !is_null($id))    {
        
            $data['title'] = "View Program Details";
            $data['programs'] = $this->program_model->get_specific($id);

            $this->templates('actions/view_program', $data);

        } else  {

            show_404();

        }

    }

    public function edit($id = NULL)  {

        $fileupload = $this->input->post('fileupload');
        
        $data['title'] = "Edit Program Details";
        $data['courses'] = $this->program_model->get_course();
        $data['areas'] = $this->db->get('sqa_areas')->result();
        $data['programs'] = $this->program_model->get_specific($id);

        // upload config
        $file_path = realpath(APPPATH. '../uploads/files');

        $config['upload_path'] = $file_path;
        $config['allowed_types'] = 'pdf';
        $config['remove_spaces'] = true;

        $this->upload->initialize($config);
        // end upload

        if(is_numeric($id) && !is_null($id))    {

            $this->form_validation->set_rules('courses', 'degree program', 'required');
            $this->form_validation->set_rules('areas', 'areas', 'required');

            if($this->input->post('phases') != 0)    {

                $this->form_validation->set_rules('levels', 'level', 'required');

            } 

            if($this->form_validation->run() == FALSE)    {

                $this->templates('actions/edit_program', $data);

            } else  {
                if(!$this->upload->do_upload('fileupload'))    {

                    if($this->program_model->update_failed($id))    {
    
                        $message = "<p class='alert alert-success'>Successfully updated the program.</p>";
                        
                    } else{
    
                        $message = "<p class='alert alert-danger'>Failed to update the program</p>";
    
                    }
    
                } else  {
    
                    if($this->program_model->update($id))    {
    
                        $message = "<p class='alert alert-success'>Successfully updated the program.</p>";
                        
                    } else{
    
                        $message = "<p class='alert alert-danger'>Failed to update the program</p>";
    
                    }
    
                }
    
                $this->session->set_flashdata('message', $message);
                redirect('accreditation/programs', 'refresh');
            }

        } else  {

            show_404();

        }

    }

    public function delete()    {

        $id = intval($this->input->post('pid', TRUE));
        $file_path = realpath(APPPATH. '../uploads/files');

        if(is_numeric($id) && !is_null($id))    {

            $data['programs'] = $this->program_model->get_specific($id);

            if($this->program_model->delete_prog($id))    {

                foreach($data['programs'] as $program)   {

                    $fileToDelete = $file_path. "\\" .$program->fileupload;
                    unlink($fileToDelete);

                }

                $response['status'] = 'success';
                $response['message'] = 'Dispense deleted successfully';

            } else  {

                $response['status'] = 'error';
                $response['message'] = 'Unable to delete dispense';

            }


        } else  {

            show_404();

        }

        echo json_encode($response);

    }

}
