<?php $users = $this->session->userdata('users'); ?>

<?php foreach($users as $user): ?>

<div class="jumbotron jumbotron-fluid bg-primary text-white">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <h1>Self Survey</h1>
            </div>
        </div>
    </div>
</div>

<!-- adding a programs modal -->
<div class="modal fade create-survey" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create Self Survey</h5>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer"></div>    
        </div>
    </div>
</div>
<!-- End modal -->

<div class="container mt-5">
    <div class="row">
        <div class="col-md-12">
            <a href="#" class="btn btn-success" data-toggle="modal" data-target=".create-survey"><span class="fa fa-plus-circle"></span> Add Survey</a>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md-12">
            <table id="programs" class="table table-striped table-hovers" style="width:100%;">
                <thead class="bg-secondary text-white">
                    <tr class="text-center">
                        <th>ID</th>
                        <th>Name of the Program</th>
                        <th>College</th>
                        <th>View</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="text-center">
                        <td>1</td>
                        <td>Bachelor's of Science in Computer Science</td>
                        <td>College of Engineering, Computer Studies and Technology</td>
                        <td><a href="#" class="btn btn-info btn-sm" title="View"><i class="fa fa-info-circle"></a></td>
                        <td><a href="#" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></a></td>
                        <td><a href="#" class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-trash"></a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php endforeach; ?>
