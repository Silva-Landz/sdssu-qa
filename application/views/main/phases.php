
<?php $users = $this->session->userdata('users'); ?>

<?php foreach($users as $user): ?>

<div class="jumbotron jumbotron-fluid bg-primary text-white">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <h1>Phases</h1>
            </div>
        </div>
    </div>
</div>


<div class="container mt-5">

    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-pills justify-content-center mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Preliminary</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Phase 1</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Phase 2</a>
                </li>
            </ul>
            <!-- Preliminary -->
            <div class="tab-content mb-5" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <table id="preliminary" class="table table-striped table-hovers" style="width:100%;">
                        <thead class="bg-secondary text-white">
                            <tr class="text-center">
                                <th>ID</th>
                                <th>AREA</th>
                                <th>Degree Program</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(is_object($programs) || is_array($programs)):?>
                                <?php foreach($programs as $program): ?>
                                    <?php if($program->phase == 0):?>
                                    <tr>
                                        <td><?= $program->prog_id; ?></td>
                                        <td><?= $program->area_name; ?></td>
                                        <td><?= $program->course_name; ?></td>
                                    </tr>
                                    <?php endif;?>
                                <?php endforeach; ?>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
                <!-- Phase 1 -->
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <table id="phaseOne" class="table table-striped table-hovers" style="width:100%;">
                        <thead class="bg-secondary text-white">
                            <tr class="text-center">
                                <th>ID</th>
                                <th>AREA</th>
                                <th>Degree Program</th>
                                <th>Level</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(is_object($programs) || is_array($programs)):?>
                                <?php foreach($programs as $program): ?>
                                    <?php if($program->phase == 1):?>
                                        <tr>
                                            <td><?= $program->prog_id; ?></td>
                                            <td><?= $program->area_name; ?></td>
                                            <td><?= $program->course_name; ?></td>
                                            <td><?= $program->level; ?></td>
                                        </tr>
                                    <?php endif;?>
                                <?php endforeach; ?>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
                <!-- Phase 2 -->
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <table id="phaseTwo" class="table table-striped table-hovers" style="width:100%;">
                        <thead class="bg-secondary text-white">
                            <tr class="text-center">
                                <th>ID</th>
                                <th>AREA</th>
                                <th>Degree Program</th>
                                <th>Level</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(is_object($programs) || is_array($programs)):?>
                                <?php foreach($programs as $program): ?>
                                    <?php if($program->phase == 2):?>
                                        <tr>
                                            <td><?= $program->prog_id; ?></td>
                                            <td><?= $program->area_name; ?></td>
                                            <td><?= $program->course_name; ?></td>
                                            <td><?= $program->level; ?></td>
                                        </tr>
                                    <?php endif;?>
                                <?php endforeach; ?>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="row mt-5">
        <div class="col-md-12">
            <table id="programs" class="table table-striped table-hovers" style="width:100%;">
                <thead class="bg-secondary text-white">
                    <tr class="text-center">
                        <th>ID</th>
                        <th>AREA</th>
                        <th>Degree Program</th>
                        <th>Level</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(is_object($programs) || is_array($programs)):?>
                        <?php foreach($programs as $program): ?>
                        <tr>
                            <td><?= $program->prog_id; ?></td>
                            <td><?= $program->area_name; ?></td>
                            <td><?= $program->course_name; ?></td>
                            <td></td>
                        </tr>
                        <?php endforeach; ?>
                    <?php endif;?>
                </tbody>
            </table>
        </div>
    </div> -->

</div>

<?php endforeach; ?>
