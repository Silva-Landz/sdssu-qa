<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Areas_model extends CI_Model  {

    public function __construct()   {

        parent::__construct();

    }

    public function get_course_areas($id)  {

        $this->db->select('*')->from('sqa_programs');

        $this->db->join('sqa_course', 'sqa_course.course_id = sqa_programs.course_id');
        $this->db->join('sqa_college', 'sqa_college.college_id = sqa_course.college_id');
        $this->db->join('sqa_areas', 'sqa_areas.area_id = sqa_programs.area_id');
        $this->db->join('sqa_others', 'sqa_others.other_id = sqa_programs.other_id');
        
        $this->db->where('sqa_programs.course_id', $id);

        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->result() : false;

    }

    public function get_specific_course($id)   {

        $this->db->where('course_id', $id);

        $query = $this->db->get('sqa_course');

        return ($query->num_rows() > 0) ? $query->result() : false;

    }

    // public function get_ppp($id)   {

    //     $this->db->where('');

    // }

} 