$(document).ready(function()    {

    deleteProgram();
    deleteOther();

}); 

function deleteProgram()    {
    $(document).on('click', '.delete_prog', function(e)   {
        
        var pid = $(this).attr('data-id');
        ProgramDelete(pid);
        e.preventDefault();
    });

    function ProgramDelete(pid)   {
        swal({
            title: "Are you sure?",
            text: "You want to delete this data?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-primary",
            confirmButtonText: "Confirm",
            closeOnConfirm: false,
        }, function(isConfirm)   {
            if(isConfirm)   {
                $.ajax({
                    url: base_url + "programs/delete",
                    method: "POST",
                    data: {"pid":pid},
                    dataType:"json",
                    success: function () {
                        swal("Done!", "It was succesfully deleted!", "success");
                        location.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error deleting!", "Please try again", "error");
                    }
                });
            } else  {
                return;
            }
        });
    }
}

function deleteOther()  {
    $(document).on('click', '#delete_file', function(e)   {

        var fid = $(this).attr('data-id');
        ProgramDelete(fid);
        e.preventDefault();
    });

    function ProgramDelete(fid)   {
        swal({
            title: "Are you sure?",
            text: "You want to delete this data?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-primary",
            confirmButtonText: "Confirm",
            closeOnConfirm: false,
        }, function(isConfirm)   {
            if(isConfirm)   {
                $.ajax({
                    url: base_url + "others/delete",
                    method: "POST",
                    data: {"fid":fid},
                    dataType:"json",
                    success: function () {
                        swal("Done!", "It was succesfully deleted!", "success");
                        location.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error deleting!", "Please try again", "error");
                    }
                });
            } else  {
                return;
            }
        });
    }
}
