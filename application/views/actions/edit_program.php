<?php foreach($programs as $program): ?>
<form action="<?= base_url('programs/edit/'). $program->prog_id; ?>" method="POST" enctype="multipart/form-data">    
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="card border-success">
                    <div class="card-header bg-success text-white" style="border-radius:0px">
                        <b>Edit Programs</b>
                    </div>
                    <div class="card-body">
                        <label for=""><b>Degree Program</b></label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">Options</label>
                            </div>
                            <select class="custom-select" name="courses">
                                <option value="">Choose...</option>
                                <?php if(is_object($courses) || is_array($courses)):?>
                                    <?php foreach($courses as $course): ?>
                                        <?php if($program->course_id == $course->course_id): ?>
                                            <option value="<?= $program->course_id; ?>" selected="selected"><?= $program->course_name; ?></option>
                                        <?php else: ?>
                                            <option value="<?= $course->course_id; ?>"><?= $course->course_name; ?></option>    
                                        <?php endif; ?>
                                        
                                    <?php endforeach; ?>
                                <?php endif;?>
                            </select>
                        </div>
                        <small class="form-text text-danger"><b><?= form_error('courses'); ?></b></small>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for=""><b>AREAS</b></label>
                                <select name="areas" id="" class="form-control">
                                    <option value="">Choose...</option>
                                    <?php if(is_object($areas) || is_array($areas)):?>
                                        <?php foreach($areas as $area): ?>
                                            <?php if($program->area_id == $area->area_id): ?>
                                                <option value="<?= $program->area_id; ?>" selected="selected"><?= $program->area_name; ?></option>
                                            <?php else: ?>
                                                <option value="<?= $area->area_id; ?>"><?= $area->area_name; ?></option>    
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif;?>
                                </select>
                            </div>

                            <div class="col-md-3">
                                <label for=""><b>Level</b></label>
                                <select name="levels" class="form-control">
                                    <option value="">Choose</option>
                                    <option value="1" <?php if($program->level == 1) echo "selected='selected'";?>>Level 1</option>
                                    <option value="2" <?php if($program->level == 2) echo "selected='selected'";?>>Level 2</option>
                                    <option value="3" <?php if($program->level == 3) echo "selected='selected'";?>>Level 3</option>
                                    <option value="4" <?php if($program->level == 4) echo "selected='selected'";?>>Level 4</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for=""><b>Phase</b></label>
                                <select name="phases" class="form-control">
                                    <option value="">Choose</option>
                                    <option value="0" <?php if($program->phase == 0) echo "selected='selected'";?>>Preliminary</option>
                                    <option value="1" <?php if($program->phase == 1) echo "selected='selected'";?>>Phase 1</option>
                                    <option value="2" <?php if($program->phase == 2) echo "selected='selected'";?>>Phase 2</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row mt-5">
                            <div class="col-md-6">
                                <label for=""><i class="fa fa-upload"></i> <b>Upload file</b></label>
                                <input type="file" class="form-control" name="fileupload" value="<?= $program->fileupload; ?>">
                                <input type="hidden" name="fileupload_hidden" value="<?= $program->fileupload; ?>">
                            </div>
                            <div class="col-md-6">
                                <label for=""></label>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" name="submit" class="btn btn-primary pull-right">Update</button>
                        <a href="<?= base_url('accreditation/programs'); ?>" class="btn btn-light mr-2 pull-right">Cancel</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</form>
<?php endforeach; ?>