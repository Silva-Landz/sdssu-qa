<?php $users = $this->session->userdata('users'); ?>
<?php foreach($users as $user): ?>

<div class="jumbotron jumbotron-fluid bg-primary text-white">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <h1>Programs</h1>
            </div>
        </div>
    </div>
</div>

<div class="container mt-5">
    <div class="row">
        <div class="col-md-12">
            <?= $this->session->flashdata('message'); ?>
            <a href="<?= base_url('programs/create'); ?>" class="btn btn-success"><span class="fa fa-plus-circle"></span> Add Program</a>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md-12">
            <table id="programs" class="table table-striped table-hovers" style="width:100%;">
                <thead class="bg-secondary text-white">
                    <tr class="text-center">
                        <th>ID</th>
                        <th>Name of the Program</th>
                        <th>College</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(is_object($programs) || is_array($programs)):?>
                        <?php foreach($programs as $program): ?>
                        <tr>
                            <td><?= $program->area_id; ?></td>
                            <td><?= $program->course_name; ?></td>
                            <td><?= $program->college_code; ?></td>
                            <td><a href="<?= base_url('programs/view/'). $program->area_id; ?>" class="btn btn-info btn-sm" title="View"><i class="fa fa-info-circle"></a></td>
                            <td><a href="#" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></a></td>
                            <td><a href="#" class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-trash"></a></td>
                        </tr>
                        <?php endforeach; ?>
                    <?php endif;?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php endforeach; ?>
