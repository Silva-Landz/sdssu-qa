<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Program_model extends CI_Model  {

    public function get_all()   {

        $this->db->select('*');
        $this->db->from('sqa_programs');

        $this->db->join('sqa_areas', 'sqa_areas.area_id=sqa_programs.area_id');
        $this->db->join('sqa_course', 'sqa_programs.course_id=sqa_course.course_id');
        $this->db->join('sqa_college', 'sqa_college.college_id=sqa_course.college_id');
        
        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->result() : false;

    }

    public function get_course()    {

        $this->db->order_by('course_name', 'ASC');

        $query = $this->db->get('sqa_course');
        
        return ($query->num_rows() > 0) ? $query->result() : false;
    }

    public function create($file = array())    {

        $data = array(
            'course_id'     => $this->input->post('courses', true),
            'area_id'       => $this->input->post('areas', true),
            'level'         => $this->input->post('levels', true),
            'phase'         => $this->input->post('phases', true),
            'other_id'      => $this->input->post('ppp', true),
            'fileupload'    => $file['file_name']
        );

        $this->db->insert('sqa_programs', $data);

        return true;

    }

    public function get_specific($id)  {

        $this->db->select('*');
        $this->db->from('sqa_areas');

        $this->db->join('sqa_programs', 'sqa_areas.area_id=sqa_programs.area_id');
        $this->db->join('sqa_course', 'sqa_programs.course_id=sqa_course.course_id');
        $this->db->join('sqa_college', 'sqa_college.college_id=sqa_course.college_id');

        $this->db->where('prog_id', $id);
        
        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->result() : false;

    }

    public function delete_prog($id)   {

        $this->db->where('prog_id', $id);
        $this->db->delete('sqa_programs');

        return true;

    }

    // if the uploaded file is change
    public function update($id)    {
        
        $file_path = realpath(APPPATH. '../uploads/files');
        $file_name = $this->upload->data();

        $data['programs'] = $this->get_specific($id);

        foreach($data['programs'] as $program)   {

            $fileToDelete = $file_path. "\\" .$program->fileupload;
            unlink($fileToDelete);

        }

        $data = array(
            'course_id'     => $this->input->post('courses', true),
            'area_id'       => $this->input->post('areas', true),
            'level'         => $this->input->post('levels', true),
            'phase'         => $this->input->post('phases', true),
            'other_id'      => $this->input->post('ppp', true),
            'fileupload'    => $file_name['file_name']
        );

        $this->db->where('prog_id', $id);
        $this->db->update('sqa_programs', $data);

        return true;

    }

    // if the there's no changes in file that you upload
    public function update_failed($id) {

        $file_name = $this->input->post('fileupload_hidden');

        $data = array(
            'course_id'     => $this->input->post('courses', true),
            'area_id'       => $this->input->post('areas', true),
            'level'         => $this->input->post('levels', true),
            'phase'         => $this->input->post('phases', true),
            'other_id'      => $this->input->post('ppp', true),
            'fileupload'    => $file_name
        );

        $this->db->where('prog_id', $id);
        $this->db->update('sqa_programs', $data);

        return true;

    }

}