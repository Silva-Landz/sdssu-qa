<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Areas extends CI_Controller {

	public function __construct()   {

        parent::__construct();

        $this->load->model('areas_model');

    }

    public function templates($path, $data = NULL) {

        if($this->session->userdata('isLoggedIn') == 1 || $this->uri->segment(2) == 'login' || $this->uri->segment(2) == 'register')    {

            $this->load->view('includes/header', $data);
            $this->load->view($path, $data);
            $this->load->view('includes/footer');

        } else {

            show_404();

        }

    }

    public function view($id = NULL)  {

        $data['title'] = "Programs and Areas";

        $data['courses'] = $this->areas_model->get_specific_course($id);
        $data['areas'] = $this->areas_model->get_course_areas($id);
        // $data['ppp'] = $this->areas_model->get_ppp($id);

        if(isset($id) && is_numeric($id) && !empty($id))    {

            $this->templates('actions/view_areas', $data);

        } else  {

            show_404();

        }
    }
}