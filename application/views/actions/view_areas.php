 <!-- Course -->
 <?php if(is_array($courses) || is_object($courses)):?>
    <?php foreach($courses as $course):?>

    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12 text-right"><a href="<?= base_url('admin/dashboard'); ?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back</a></div>
            <div class="col-md-12 mt-4"><h3><b><?= $course->course_name; ?></b></h3></div>
        </div>
        <hr>
    </div>

    <?php endforeach; ?>
<?php endif;?>

 <!-- The area file and the ppp file -->

<?php if(is_array($areas) || is_object($areas)):?>
    <?php foreach($areas as $area):?>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <caption><p><b><?= $area->area_name; ?></b></p></caption>
                <ul>
                    <li>Filename: <a href="<?= base_url('uploads/files/'). $area->fileupload;?>"><?= $area->fileupload; ?></a></li>
                    <li>PPP: <a href="<?= base_url('uploads/files/others/'). $area->other_fileupload;?>"><?= $area->other_name; ?></a></li>
                </ul>
            </div>
        </div>
    </div>

    <?php endforeach; ?>
<?php else:?>
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12 text-center">
                <h1 class="display-1"><i class="fa fa-frown-o"></i><br>
                    No Content
                </h1>
            </div>
        </div>
    </div>

<?php endif; ?>