<?php if(is_array($others) || is_object($others)):?>
    <?php foreach($others as $other):?>
        
    <form action="<?= base_url('others/edit/'). $other->other_id; ?>" method="POST" enctype="multipart/form-data">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mt-5">
                    <div class="card border-success">
                        <div class="card-header bg-success text-white">
                            <b>Edit <?= $other->other_name; ?></b>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-md-8">
                                    <label for=""><b>File Description</b></label>
                                    <input type="text" class="form-control" name="description" value="<?= $other->other_name; ?>" placeholder="Enter description">
                                    <small class="text-danger"><?= form_error('description'); ?></small>
                                </div>
                                <div class="col-md-4">
                                    <label for=""><b>Category</b></label>
                                    <select name="categories" class="form-control">
                                        <option value="">Choose...</option>
                                        <?php if(is_object($categories) || is_array($categories)):?>
                                            <?php foreach($categories as $category): ?>
                                                <?php if($category->cat_id == $other->cat_id):?>
                                                    <option value="<?= $other->cat_id; ?>" selected="selected"><?= $other->cat_name; ?></option> 
                                                <?php else:?>
                                                    <option value="<?= $category->cat_id; ?>"><?= $category->cat_name; ?></option>
                                                <?php endif; ?>
                                                      
                                            <?php endforeach; ?>
                                        <?php endif;?>
                                    </select>
                                    <small class="text-danger"><?= form_error('categories'); ?></small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label for=""><b>Levels</b></label>
                                    <select name="levels" class="form-control">
                                        <option value="">Choose</option>
                                        <option value="1" <?php if($other->other_level == 1) echo "selected='selected'"; ?>>Level 1</option>
                                        <option value="2" <?php if($other->other_level == 2) echo "selected='selected'"; ?>>Level 2</option>
                                        <option value="3" <?php if($other->other_level == 3) echo "selected='selected'"; ?>>Level 3</option>
                                        <option value="4" <?php if($other->other_level == 4) echo "selected='selected'"; ?>>Level 4</option>
                                    </select>
                                    <small class="text-danger"><?= form_error('levels'); ?></small>
                                </div>
                                <div class="col-md-4">
                                    <label for=""><b>Phase</b></label>
                                    <select name="phases" class="form-control">
                                        <option value="">Choose</option>
                                        <option value="0" <?php if($other->other_phase == 0) echo "selected='selected'"; ?>>Preliminary</option>
                                        <option value="1" <?php if($other->other_phase == 1) echo "selected='selected'"; ?>>Phase 1</option>
                                        <option value="2" <?php if($other->other_phase == 2) echo "selected='selected'"; ?>>Phase 2</option>
                                    </select>
                                    <small class="text-danger"><?= form_error('phases'); ?></small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label for=""><b>Browse file:</b></label>
                                    <input type="file" class="form-control" name="others_upload">
                                    <input type="hidden" name="fileupload_hidden" value="<?= $other->other_fileupload; ?>">
                                    <small class="form-text"><b>Note:</b> Upload PDF files only.</small>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary pull-right">Update entry</button>
                            <a href="<?= base_url('accreditation/others'); ?>" class="btn btn-light pull-right mr-2">Cancel</a>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </form>

    <?php endforeach; ?>
<?php endif; ?>