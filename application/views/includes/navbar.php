<?php $users = $this->session->userdata('users'); ?>
<?php foreach($users as $user): ?>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary" style="background-color: #34495e !important">
    <a class="navbar-brand" href="#"><small>(Logo here) </small></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="nav navbar-nav mr-auto">
            <li class="nav-item ml-auto">
                <?php if($this->uri->segment(2) == 'dashboard'): ?>
                    <a class="nav-link active" href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i> Home<span class="sr-only">(current)</span></a>
                <?php else:?>
                    <a class="nav-link" href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i> Home <span class="sr-only">(current)</span></a>
                <?php endif; ?>
            </li>
            <li class="nav-item">
                <?php if($this->uri->segment(2) == 'iso'): ?>
                    <a class="nav-link active" href="<?= base_url('admin/iso'); ?>">ISO</a>
                <?php else:?>  
                    <a class="nav-link" href="<?= base_url('admin/iso'); ?>">ISO <span class="sr-only">(current)</span></a>  
                <?php endif; ?>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Accreditation
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="<?= base_url('accreditation/programs'); ?>">Programs</a>
                    <?php if($user->usertype == "administrator"): ?>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?= base_url('accreditation/others'); ?>">Others</a>
                        <a class="dropdown-item" href="<?= base_url('accreditation/phases'); ?>">Phases</a>
                    <?php endif; ?>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">About Us</a>
            </li>
        </ul>
        <ul class="nav navbar-nav mr-3">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-user"></span> <?= $user->firstname ." ". $user->lastname ?></a>
                <div class="dropdown-menu" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="#">Change Password</a>
                    <a class="dropdown-item" href="<?= base_url('admin/logout'); ?>">Logout</a>
                </div>
            </li>
        </ul>
    </div>
</nav>

<?php endforeach; ?>