<?php $users = $this->session->userdata('users'); ?>

<?php foreach($users as $user): ?>

<div class="jumbotron jumbotron-fluid bg-primary text-white">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <h1>ISO - International Organization for Standardization</h1>
            </div>
        </div>
    </div>
</div>

<div class="container mt-5">
    <div class="row">
        <div class="col-md-12">
            <p class="alert alert-info"><i class="fa fa-info-circle"></i>
                <b>Notice: </b> The Surigao del sur State University ISO format will be available this coming September 2018.
            </p>
            
            <h1 class="text-center display-2 text-warning"><i class="fa fa-warning"></i></h1>
            <h1 class="text-center display-4"><b>Coming Soon</b></h1>
            <p class="text-center lead">Page is under maintenance</p>
        </div>
    </div>
</div>

<?php endforeach; ?>
