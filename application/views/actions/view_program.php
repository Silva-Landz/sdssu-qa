<?php if(is_array($programs) || is_object($programs)):?>
    <?php foreach($programs as $program):?>
        
        <!-- Content -->
        <div class="container">
            <div class="row mt-3">
                <div class="col-md-12">
                    <a href="<?= base_url('accreditation/programs'); ?>" class="btn btn-dark mb-3"><i class="fa fa-arrow-left"></i> Back</a>
                    <h4><b><?= $program->course_name; ?></b></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <b class="text-muted"><?= $program->area_name; ?> | 
                                    <?php if($program->phase == 0)    {
                                        echo "Preliminary";
                                    } else  {
                                        echo "Level " .$program->level. " - Phase " .$program->phase;
                                    } ?></b>
                </div>
                <div class="col-md-12 mt-4">
                    <embed src="<?= base_url('uploads/files/'). $program->fileupload; ?>" type="application/pdf" width="100%" height="600px" title="None">
                </div>
            </div>
            <div class="row mt-3 mb-5">
                <div class="col-md-12">
                <a href="<?= base_url('uploads/files/'). $program->fileupload; ?>" class="btn btn-success btn-sm pull-right"><i class="fa fa-download"></i> Download</a>
                </div>
            </div>
        </div>


    <?php endforeach; ?>
<?php endif; ?>