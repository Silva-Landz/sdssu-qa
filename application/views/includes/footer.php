    

    <?php if($this->uri->segment(2) != 'login'):  ?>
        <div class="container">
            <div class="row mt-2">
                <hr>
                <div class="col-md-12 text-center">
                    <hr>
                    <p class="text-muted">All Rights Reserved. Copyright <?= date('Y'); ?> <br>
                        Surigao del sur State University Quality Assurance Archiving System
                    </p>
                    
                    <p>version 1.0</p>
                </div>
            </div>
        </div>
    <?php endif;  ?>

    <script>var base_url = "<?= base_url(); ?>";</script>
    <script src="<?= base_url('assets/js/jquery-3.3.1.js'); ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('assets/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?= base_url('assets/js/dataTables.bootstrap4.min.js'); ?>"></script>
    <script src="<?= base_url('assets/js/sweetalert.js'); ?>"></script>
    <script src="<?= base_url('assets/js/sweetalert.min.js'); ?>"></script>
    <script src="<?= base_url('assets/js/custom.js'); ?>"></script>
    <!-- <script src="<?= base_url('assets/ckeditor/ckeditor.js'); ?>"></script> -->
    <script>
        $(document).ready(function() {
            $('#programs').DataTable();
            $('#preliminary').DataTable();
            $('#phaseOne').DataTable();
            $('#phaseTwo').DataTable();
            $('.collapse').collapse();
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
</body>
</html>