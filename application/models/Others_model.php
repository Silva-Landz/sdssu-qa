<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Others_model extends CI_Model  {

    public function __construct()   {

        parent::__construct();

    }

    public function get_all()   {

        $this->db->select('*')->from('sqa_others');
        $this->db->join('sqa_categories', 'sqa_others.cat_id=sqa_categories.cat_id');

        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->result() : false;

    }

    public function get_specific($id)  {

        $this->db->select('*')->from('sqa_others');
        $this->db->join('sqa_categories', 'sqa_categories.cat_id = sqa_others.cat_id');

        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->result() : false;

    }

    public function create($file = array())   {

        $data = array(
            'other_name'       => $this->input->post('description', true), 
            'cat_id'           => $this->input->post('categories', true), 
            'other_level'      => $this->input->post('levels', true), 
            'other_phase'      => $this->input->post('phases', true), 
            'other_fileupload' => $file['file_name'] 
        );

        $this->db->insert('sqa_others', $data);

        return true;

    }

    public function update($id)    {

        $file_path = realpath(APPPATH. '../uploads/files/others');
        $file_name = $this->upload->data();

        $data['others'] = $this->get_specific($id);

        foreach($data['others'] as $other)   {

            $fileToDelete = $file_path. "\\" .$other->other_fileupload;
            unlink($fileToDelete);

        }

        $data = array(
            'other_name'       => $this->input->post('description', true), 
            'cat_id'           => $this->input->post('categories', true), 
            'other_level'      => $this->input->post('levels', true), 
            'other_phase'      => $this->input->post('phases', true), 
            'other_fileupload' => $file_name['file_name'] 
        );

        $this->db->where('other_id', $id);
        $this->db->update('sqa_others', $data);

        return true;

    }

    public function update_failed($id)    {

        $file_name = $this->input->post('fileupload_hidden');

        $data = array(
            'other_name'       => $this->input->post('description', true), 
            'cat_id'           => $this->input->post('categories', true), 
            'other_level'      => $this->input->post('levels', true), 
            'other_phase'      => $this->input->post('phases', true), 
            'other_fileupload' => $file_name 
        );

        $this->db->where('other_id', $id);
        $this->db->update('sqa_others', $data);

        return true;

    }

    public function delete_file($id)   {

        $this->db->where('other_id', $id);
        $this->db->delete('sqa_others');

        return true;

    }
}