<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()   {

        parent::__construct();

        $this->load->model('admin_model');
        $this->load->model('program_model');

    }

    public function templates($path, $data = NULL) {

        if($this->session->userdata('isLoggedIn') == 1 || $this->uri->segment(2) == 'login' || $this->uri->segment(2) == 'register')    {

            $this->load->view('includes/header', $data);
            $this->load->view($path, $data);
            $this->load->view('includes/footer');

        } else {

            show_404();

        }

    }

    public function login() {

        $data['title'] = 'Login | QA Archiving Information System';

        // setting form rules for login
        $this->form_validation->set_rules('username', 'username', 'required|trim');
        $this->form_validation->set_rules('password', 'password', 'required|trim|min_length[5]|callback_validate');

        if($this->form_validation->run() == FALSE)    {

            $this->templates('accounts/login', $data);

        } else  {

            $data = array(
                'users'         => $this->admin_model->login_account(),
                'isLoggedIn'    => 1 
            );

            $this->session->set_userdata($data);

            redirect('admin/dashboard');

        }
    }

    public function validate()  {

        if($this->admin_model->login_account())    {

            return TRUE;

        } else {

            $this->form_validation->set_message('validate', 'Incorrect username or password or usertype');

            return FALSE;

        }

    }

    public function register()  {

        $data['title'] = "Register Account";

        // setting form rules
        $this->form_validation->set_rules('firstname', 'firstname', 'required');
        $this->form_validation->set_rules('lastname', 'lastname', 'required');
        $this->form_validation->set_rules('username', 'username', 'required|trim');
        $this->form_validation->set_rules('password', 'password', 'required|trim|min_length[5]');
        $this->form_validation->set_rules('confpass', 'password confirmation', 'required|trim|min_length[5]|matches[password]');
        $this->form_validation->set_rules('usertype', 'usertype', 'required');

        if($this->form_validation->run() == FALSE)    {

            $this->templates('accounts/register', $data);

        } else {

            $message = '<p class="alert alert-success"><i class="fa fa-check-circle"></i> Successfully registered your account..</p>';

            if($this->admin_model->register_account() == TRUE)    {

                $this->session->set_flashdata('success', $message);
            
                redirect('admin/register');

            }

            return FALSE;
        }

    }

    public function dashboard() {

        $data['title'] = "QA | Dashboard";
        $data['courses'] = $this->program_model->get_course();

        $this->templates('main/dashboard', $data);

    }

    public function ISO()   {

        $data['title'] = "QA | ISO";

        $this->templates('main/iso', $data);

    }

    public function logout()   {

        $this->session->sess_destroy();

        redirect('admin/login');

    }

}
