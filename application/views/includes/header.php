<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $title; ?></title>

    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/dataTables.bootstrap4.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css'); ?>">
</head>
<body>

    <?php if($this->uri->segment(2) == 'dashboard'): ?>
            <?php $this->load->view('includes/navbar'); ?>
    <?php elseif($this->uri->segment(2) == 'iso'): ?>
            <?php $this->load->view('includes/navbar'); ?>  
    <?php elseif($this->uri->segment(2) == 'programs'): ?>
            <?php $this->load->view('includes/navbar'); ?>  
    <?php elseif($this->uri->segment(2) == 'others'): ?>
            <?php $this->load->view('includes/navbar'); ?>
    <?php elseif($this->uri->segment(2) == 'phases'): ?>
            <?php $this->load->view('includes/navbar'); ?>
    <?php endif; ?>
