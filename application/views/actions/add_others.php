<form action="<?= base_url('others/create'); ?>" method="POST" enctype="multipart/form-data">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-5">
                <div class="card border-success">
                    <div class="card-header bg-success text-white">
                        <b>Create new entry</b>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-md-8">
                                <label for=""><b>File Description</b></label>
                                <input type="text" class="form-control" name="description" value="<?= set_value('description'); ?>" placeholder="Enter description">
                                <small class="text-danger"><?= form_error('description'); ?></small>
                            </div>
                            <div class="col-md-4">
                                <label for=""><b>Category</b></label>
                                <select name="categories" class="form-control">
                                    <option value="">Choose...</option>
                                    <?php if(is_object($categories) || is_array($categories)):?>
                                        <?php foreach($categories as $category): ?>
                                            <option value="<?= $category->cat_id; ?>"><?= $category->cat_name; ?></option>  
                                        <?php endforeach; ?>
                                    <?php endif;?>
                                </select>
                                <small class="text-danger"><?= form_error('categories'); ?></small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4">
                                <label for=""><b>Levels</b></label>
                                <select name="levels" class="form-control">
                                    <option value="">Choose</option>
                                    <option value="1">Level 1</option>
                                    <option value="2">Level 2</option>
                                    <option value="3">Level 3</option>
                                    <option value="4">Level 4</option>
                                </select>
                                <small class="text-danger"><?= form_error('levels'); ?></small>
                            </div>
                            <div class="col-md-4">
                                <label for=""><b>Phase</b></label>
                                <select name="phases" class="form-control">
                                    <option value="">Choose</option>
                                    <option value="0">Preliminary</option>
                                    <option value="1">Phase 1</option>
                                    <option value="2">Phase 2</option>
                                </select>
                                <small class="text-danger"><?= form_error('phases'); ?></small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for=""><b>Upload file</b></label>
                                <input type="file" class="form-control" name="others_upload">
                                <small class="form-text"><b>Note:</b> Upload PDF files only.</small>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary pull-right">Submit entry</button>
                        <a href="<?= base_url('accreditation/others'); ?>" class="btn btn-light pull-right mr-2">Cancel</a>
                    </div>
                </div>
            
            </div>
        </div>
    </div>
</form>