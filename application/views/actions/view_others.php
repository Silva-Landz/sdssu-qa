<?php if(is_array($others) || is_object($others)):?>
    <?php foreach($others as $other):?>
        
        <!-- Content -->
        <div class="container">
            <div class="row mt-3">
                <div class="col-md-12">
                    <a href="<?= base_url('accreditation/others'); ?>" class="btn btn-dark mb-3"><i class="fa fa-arrow-left"></i> Back</a>
                    <h4><b><?= $other->other_name; ?></b><a href="<?= base_url('others/edit/'). $other->other_id; ?>" class="text-secondary ml-3" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <b class="text-muted"> <?= $other->cat_name; ?> |
                                    <?php if($other->other_phase == 0)    {
                                        echo "Preliminary";
                                    } else  {
                                        echo "Level " .$other->other_level. " - Phase " .$other->other_phase;
                                    } ?> 
                    <small>| Last Modified: <?= $other->other_registered_on; ?></small>    
                    </b> 

                </div>
                <div class="col-md-12 mt-4">
                    <embed src="<?= base_url('uploads/files/others/'). $other->other_fileupload; ?>" type="application/pdf" width="100%" height="600px" title="None">
                </div>
            </div>
            <div class="row mt-3 mb-5">
                <div class="col-md-12">
                <a href="<?= base_url('uploads/files/others/'). $other->other_fileupload; ?>" class="btn btn-success btn-sm pull-right"><i class="fa fa-download"></i> Download</a>
                </div>
            </div>
        </div>

    <?php endforeach; ?>
<?php endif; ?>